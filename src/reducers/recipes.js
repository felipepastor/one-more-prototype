import {
  FETCH_RECIPES_BEGIN,
  FETCH_RECIPES_SUCCESS,
  FETCH_RECIPES_FAILURE,
  TOGGLE_FAVORITE,
  SET_VISIBILITY_FILTER,
} from '../actions';

/**
 *
 * @type {{items: Array, original: Array, loading: boolean, error: null}}
 */
const initialState = {
  items: [],
  original: [],
  loading: false,
  error: null,
};

/**
 *
 * @param state
 * @param action
 * @returns {{items: Array, original: Array, loading: boolean, error: null}}
 */
const recipes = (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_FAVORITE:
      let favorites = JSON.parse(
        window.localStorage.getItem('favorites') || '[]'
      );

      if (!favorites && !Array.isArray(favorites)) {
        favorites = [];
      }

      state.original = state.original.map(recipe => {
        if (recipe.id === action.id) {
          if (!recipe.isFavorite) {
            favorites.push(recipe.id);
            window.localStorage.setItem('favorites', JSON.stringify(favorites));
          } else {
            window.localStorage.setItem(
              'favorites',
              JSON.stringify(favorites.filter(f => f !== recipe.id))
            );
          }

          return {
            ...recipe,
            isFavorite: !recipe.isFavorite,
          };
        }
        return recipe;
      });

      return {
        ...state,
        loading: false,
        items: state.original,
      };

    case FETCH_RECIPES_BEGIN:
      return {
        ...state,
        loading: true,
        error: null,
      };

    case FETCH_RECIPES_SUCCESS:
      return {
        ...state,
        loading: false,
        items: action.payload.recipes,
      };

    case FETCH_RECIPES_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        items: [],
      };
    case SET_VISIBILITY_FILTER:
      return {
        ...state,
        loading: false,
      };
    default:
      return state;
  }
};

export default recipes;
