import { filters, SET_VISIBILITY_FILTER } from '../actions';

const initialState = {
  type: filters.SHOW_ALL,
  term: '',
  isFavoriteSearch: false,
};

/**
 *
 * @param state
 * @param action
 * @returns {*}
 */
const filter = (state = initialState, action) => {
  switch (action.type) {
    case SET_VISIBILITY_FILTER:
      return {
        type: action.filter,
        term: action.term,
        isFavoriteSearch: action.isFavoriteSearch,
      };
    default:
      return state;
  }
};

export default filter;
