import {
  FETCH_RECIPES_SUCCESS,
  TOGGLE_FAVORITE,
  CLICK_RATING,
} from '../actions';

/**
 *
 * @type {{recipe: {}, isSinglePage: boolean}}
 */
const initialState = {
  recipe: {},
  isSinglePage: true,
};

/**
 *
 * @param recipe
 * @param isSet
 * @param value
 * @returns {number}
 */
const calculateRating = (recipe, isSet, value) => {
  let ratings = JSON.parse(window.localStorage.getItem('ratings')) || [];

  if (isSet) {
    ratings.push({
      id: recipe.id,
      value: parseInt(value, 10),
    });

    window.localStorage.setItem('ratings', JSON.stringify(ratings));
  }

  let currentRating = 0;
  ratings.map(r => {
    if (r.id === recipe.id) {
      currentRating += parseInt(r.value, 10) / ratings.length;
    }
    return r;
  });

  return Math.round(currentRating);
};

/**
 *
 * @param state
 * @param action
 * @returns {*}
 */
const recipe = (state = initialState, action = { payload: {} }) => {
  switch (action.type) {
    case FETCH_RECIPES_SUCCESS:
      if (!action.payload || !action.id) return state;

      const {
        payload: { recipes = [] },
      } = action;

      let recipe = recipes[0];

      recipe.currentRating = calculateRating(recipe);

      return {
        ...recipe,
      };
    case TOGGLE_FAVORITE:
      state.isFavorite = !state.isFavorite;
      return {
        ...state,
      };
    case CLICK_RATING:
      state.currentRating = calculateRating(state, true, action.value);
      return {
        ...state,
      };
    default:
      return {};
  }
};

export default recipe;
