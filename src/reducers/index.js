import { combineReducers } from 'redux';
import recipes from './recipes';
import recipe from './recipe';
import filter from './filter';

export default combineReducers({
  recipes,
  recipe,
  filter,
});
