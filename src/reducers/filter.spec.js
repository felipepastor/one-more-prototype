import filter from './filter';
import { filters, SET_VISIBILITY_FILTER } from '../actions';

describe('filter spec', () => {
  const initialState = {
    type: filters.SHOW_ALL,
    term: '',
    isFavoriteSearch: false,
  };

  const initialAction = {
    type: SET_VISIBILITY_FILTER,
    filter: SET_VISIBILITY_FILTER,
    term: 'Crisp',
    isFavoriteSearch: false,
  };

  it('should handle initial state', () => {
    expect(filter(initialState, {})).toEqual(initialState);
  });

  it('should handle SET_VISIBILITY_FILTER', () => {
    expect(filter({}, initialAction)).toEqual({
      term: initialAction.term,
      type: SET_VISIBILITY_FILTER,
      isFavoriteSearch: false,
    });
  });
});
