import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import 'jest-localstorage-mock';
import 'mock-local-storage';
global.window = {};
window.localStorage = global.localStorage;

configure({ adapter: new Adapter() });

const localStorageMock = {
  getItem: jest.fn(),
  setItem: jest.fn(),
  clear: jest.fn(),
};

// global.navigator = {
//   // ...global.navigator,
//   userAgent: 'node.js',
//   platform: 'windows', // This can be set to mac, windows, or linux
//   appName: 'Microsoft Internet Explorer' // Be sure to define this as well
// }

window.navigator = {
  ...window.navigator,
  userAgent: 'node',
};

// global.window = {}
// global.localStorage = localStorageMock

// global.window = {
//   // ...global.document,
//   navigator: {
//     ...global.navigator,
//     userAgent: 'node'
//   }
// }
