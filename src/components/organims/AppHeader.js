import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import InputBase from '@material-ui/core/InputBase';
import { fade } from '@material-ui/core/styles/colorManipulator';
import { withStyles } from '@material-ui/core/styles';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import { withRouter } from 'react-router';
import { Paper } from '@material-ui/core';
import FavoriteButton from '../atoms/FavoriteButton';

const styles = ({
  shape,
  palette,
  breakpoints,
  transitions,
  spacing: { unit },
}) => {
  return {
    root: {
      backgroundColor: palette.primary.main,
      flexGrow: 1,
      width: '100%',
      borderRadius: 0,
      '& header': {
        maxWidth: 1280,
        margin: 'auto',
        boxShadow: 'none',
      },
    },
    grow: {
      flexGrow: 1,
    },
    rootToolbar: {
      margin: `0 -${unit}px`,
      maxWidth: 1280,
      padding: 0,
      [breakpoints.down('xs')]: {
        margin: `0 ${unit * 2}px`,
      },
    },
    toolBarInner: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      width: '100%',
      margin: `auto ${unit * 3}px`,
    },
    title: {
      display: 'none',
      [breakpoints.up('sm')]: {
        display: 'block',
      },
    },
    search: {
      position: 'relative',
      borderRadius: shape.borderRadius,
      backgroundColor: fade(palette.common.white, 0.15),
      '&:hover': {
        backgroundColor: fade(palette.common.white, 0.25),
      },
      marginRight: unit * 2,
      marginLeft: 0,
      width: '100%',
      [breakpoints.up('sm')]: {
        marginLeft: unit * 3,
        width: 'auto',
      },
    },
    searchIcon: {
      width: unit * 9,
      height: '100%',
      position: 'absolute',
      pointerEvents: 'none',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    inputRoot: {
      color: 'inherit',
      width: '100%',
    },
    inputInput: {
      paddingTop: unit,
      paddingRight: unit,
      paddingBottom: unit,
      paddingLeft: unit * 10,
      transition: transitions.create('width'),
      width: '100%',
      [breakpoints.up('md')]: {
        width: 200,
      },
    },
    sectionDesktop: {
      display: 'none',
      [breakpoints.up('md')]: {
        display: 'flex',
      },
    },
    sectionMobile: {
      display: 'flex',
      [breakpoints.up('md')]: {
        display: 'none',
      },
    },
    searchWrapper: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    },
  };
};

export class AppHeader extends React.Component {
  state = {
    searchTerm: '',
    isFavoriteSearch: false,
  };

  /**
   *
   * @param term
   */
  search(term) {
    this.setState(
      {
        searchTerm: term,
      },
      () => {
        this.props.search(this.state.searchTerm);
      }
    );
  }

  render() {
    const { classes, history, isSinglePage, search, recipe = {} } = this.props;

    const { searchTerm, isFavoriteSearch } = this.state;

    return (
      <Paper className={classes.root}>
        <AppBar position="relative">
          <Toolbar
            className={classes.toolbar}
            classes={{
              root: classes.rootToolbar,
            }}>
            {!isSinglePage ? (
              <div className={classes.toolBarInner}>
                <React.Fragment>
                  <div className={classes.search} id={'search'}>
                    <InputBase
                      placeholder="Search…"
                      classes={{
                        root: classes.inputRoot,
                        input: classes.inputInput,
                      }}
                      onChange={e => this.search(e.currentTarget.value)}
                    />
                  </div>
                  <FavoriteButton
                    id={'favoriteButton'}
                    toggleFavorite={e => {
                      this.setState(
                        {
                          isFavoriteSearch: !isFavoriteSearch,
                        },
                        () => {
                          search(searchTerm, !isFavoriteSearch);
                        }
                      );
                    }}
                    isFavorite={isFavoriteSearch}
                  />
                </React.Fragment>
              </div>
            ) : (
              <React.Fragment>
                <IconButton
                  className={classes.menuButton}
                  color="inherit"
                  aria-label="Menu"
                  onClick={() => history.goBack()}
                  id={'backButton'}>
                  <ArrowBackIcon />
                </IconButton>
                <Typography
                  className={classes.grow}
                  variant={'h5'}
                  style={{ color: 'white' }}
                  align={'center'}
                  id={'recipeName'}>
                  {recipe.name}
                </Typography>
              </React.Fragment>
            )}
          </Toolbar>
        </AppBar>
      </Paper>
    );
  }
}

AppHeader.propTypes = {
  classes: PropTypes.object.isRequired,
  history: PropTypes.shape({
    goBack: PropTypes.func.isRequired,
  }),
  isSinglePage: PropTypes.bool,
};

export default withStyles(styles)(withRouter(AppHeader));
