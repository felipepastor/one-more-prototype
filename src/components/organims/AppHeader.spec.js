import React from 'react';
import { AppHeader } from './AppHeader';
import { mount } from 'enzyme';

describe('appheader test suite', () => {
  let initialProps = {
    classes: {},
    history: {
      goBack: jest.fn(),
    },
    isSinglePage: true,
    search: '',
    recipe: {
      id: '',
      name: '',
    },
  };

  let mountedAppHeader;
  const appHeaderComponent = props => {
    if (!mountedAppHeader) {
      if (props) {
        initialProps = {
          ...initialProps,
          ...props,
        };
      }

      mountedAppHeader = mount(<AppHeader {...initialProps} />);
    }
    return mountedAppHeader;
  };

  beforeEach(() => {
    mountedAppHeader = undefined;
  });

  it('should render single header', () => {
    const appHeader = appHeaderComponent();

    expect(appHeader.find('#recipeName').length).toBeGreaterThan(0);
    expect(appHeader.find('#backButton').length).toBeGreaterThan(0);
  });

  it('should render search header', () => {
    const appHeader = appHeaderComponent({
      isSinglePage: false,
    });

    expect(appHeader.find('#favoriteButton').length).toBeGreaterThan(0);
    expect(appHeader.find('#search').length).toBeGreaterThan(0);
  });

  it('should click on backbutton and trigger history back', () => {
    const appHeader = appHeaderComponent({
      isSinglePage: true,
    });

    appHeader
      .find('#backButton')
      .find('button')
      .simulate('click');

    expect(initialProps.history.goBack).toBeCalled();
  });
});
