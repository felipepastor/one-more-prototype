import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import red from '@material-ui/core/colors/red';
import ShareIcon from '@material-ui/icons/Share';
import FavoriteButton from '../atoms/FavoriteButton';
import Truncate from 'react-truncate';
import classNames from 'classnames';

const styles = theme => ({
  card: {
    transition: 'all .2s ease-in-out',
  },
  hover: {
    transform: 'scale(1.02)',
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  actions: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  expand: {
    transform: 'rotate(0deg)',
    transition: theme.transitions.create('transform', {
      duration: theme.transitions.duration.shortest,
    }),
    marginLeft: 'auto',
    [theme.breakpoints.up('sm')]: {
      marginRight: -8,
    },
  },
  expandOpen: {
    transform: 'rotate(180deg)',
  },
  avatar: {
    backgroundColor: red[500],
  },
});

class RecipeReviewCard extends React.Component {
  state = {
    expanded: false,
    hovering: false,
  };

  hoverCard = () => {
    this.setState({
      hovering: !this.state.hovering,
    });
  };

  render() {
    const { classes, recipe = {}, toggleFavorite = () => {} } = this.props;

    const { hovering } = this.state;

    return (
      <Card
        className={classNames(classes.card, hovering && classes.hover)}
        onMouseEnter={() => this.hoverCard(!hovering)}
        onMouseLeave={() => this.hoverCard(!hovering)}>
        <Link to={{ pathname: `recipes/${recipe.id}` }}>
          <CardHeader
            avatar={
              <Avatar aria-label="Recipe" className={classes.avatar}>
                {recipe.name && recipe.name.charAt(0)}
              </Avatar>
            }
            title={<Truncate lines={1}>{recipe.name}</Truncate>}
            subheader={<Truncate lines={1}>{recipe.headline}</Truncate>}
          />
          <CardMedia
            className={classes.media}
            image={recipe.thumb}
            title={recipe.name}
          />
          <CardContent>
            <Typography component="p">
              <Truncate lines={5}>{recipe.description}</Truncate>
            </Typography>
          </CardContent>
        </Link>
        <CardActions className={classes.actions} disableActionSpacing>
          <IconButton aria-label="Share">
            <ShareIcon />
          </IconButton>
          <FavoriteButton
            toggleFavorite={toggleFavorite}
            ariaLabel={'Add to favorites'}
            isFavorite={recipe.isFavorite}
          />
        </CardActions>
      </Card>
    );
  }
}

RecipeReviewCard.propTypes = {
  classes: PropTypes.object.isRequired,
  recipe: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    headline: PropTypes.string.isRequired,
    isFavorite: PropTypes.bool.isRequired,
    thumb: PropTypes.string.isRequired,
  }),
  toggleFavorite: PropTypes.func.isRequired,
};

export default withStyles(styles)(RecipeReviewCard);
