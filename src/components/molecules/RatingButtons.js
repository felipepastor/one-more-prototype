import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

const styles = ({ palette, breakpoints, spacing: { unit } }) => ({
  starRating: {
    direction: 'rtl',
    display: 'flex',
    '& input[type = radio]': {
      display: 'none',
      '&:checked ~label': {
        color: '#f2b600',
      },
    },
    '& label': {
      color: '#bbb',
      fontSize: unit * 3,
      padding: 0,
      cursor: 'pointer',
      transition: 'all .3s ease-in-out',
      '&:hover ~label': {
        color: '#f2b600',
      },
    },
  },
});

class RatingButtons extends React.Component {
  render() {
    const { classes, onClick, recipe = {} } = this.props;

    /**
     *
     * @returns {Array}
     */
    const generateStars = () => {
      let stars = [];
      for (let i = 5; i >= 1; i--) {
        stars.push(
          <React.Fragment key={i}>
            <input
              id={`star-${i}`}
              type="radio"
              name="rating"
              value={i}
              onClick={e => {
                window.confirm(
                  `Do you really want to rating this product as ${i}?`
                ) && onClick(e.target.value);
              }}
              checked={recipe.currentRating === i}
            />
            <label htmlFor={`star-${i}`} title={`${i} stars`}>
              <i className="active fa fa-star" aria-hidden="true" />
            </label>
          </React.Fragment>
        );
      }

      return stars;
    };

    return (
      <React.Fragment>
        <div className={classes.starRating}>{generateStars()}</div>
      </React.Fragment>
    );
  }
}

RatingButtons.propTypes = {
  classes: PropTypes.object.isRequired,
  onClick: PropTypes.func.isRequired,
  recipe: PropTypes.shape({
    currentRating: PropTypes.number,
  }),
};

export default withStyles(styles)(RatingButtons);
