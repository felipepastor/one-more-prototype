import React, { Component } from 'react';
import IconButton from '@material-ui/core/IconButton';
import FavoriteIcon from '@material-ui/icons/Favorite';
import PropTypes from 'prop-types';

class FavoriteButton extends Component {
  render() {
    const {
      isFavorite,
      toggleFavorite = () => {},
      ariaLabel = '',
      gutter = '',
    } = this.props;
    const recipeRed = isFavorite ? { color: 'red' } : {};

    return (
      <IconButton
        aria-label={ariaLabel}
        onClick={toggleFavorite}
        style={{ padding: gutter }}>
        <FavoriteIcon style={recipeRed} />
      </IconButton>
    );
  }
}

FavoriteButton.propTypes = {
  isFavorite: PropTypes.bool,
  toggleFavorite: PropTypes.func.isRequired,
  gutter: PropTypes.string,
  ariaLabel: PropTypes.string,
};

export default FavoriteButton;
