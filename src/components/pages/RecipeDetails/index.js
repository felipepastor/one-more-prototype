import React, { Component } from 'react';
import { Grid, Typography } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import withWidth from '@material-ui/core/withWidth';
import Divider from '@material-ui/core/Divider';
import Paper from '@material-ui/core/Paper';
import Icon from '@material-ui/core/Icon';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import classNames from 'classnames';
import FavoriteButton from '../../atoms/FavoriteButton';
import moment from 'moment';
import momentDurationFormatSetup from 'moment-duration-format';
import RatingButtons from '../../molecules/RatingButtons';
import PropTypes from 'prop-types';

momentDurationFormatSetup(moment);

const styles = ({ palette, breakpoints, spacing: { unit } }) => ({
  imageWrapper: {
    '& img': {
      width: '100%',
    },
  },
  image: {
    display: 'flex',
    margin: 'auto',
    '& img': {
      width: '100%',
      height: '100%',
      borderRadius: unit / 2,
    },
  },
  paper: {
    padding: unit * 2,
  },
  titleWrapper: {
    display: 'flex',
    alignItems: 'center',
  },
  infoWrapper: {
    padding: `${unit * 2}px 0`,
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  divider: {
    margin: `${unit * 2}px 0`,
  },
  iconLeft: {
    width: unit * 5.25,
  },
  basicInfo: {
    display: 'flex',
    alignItems: 'end',
    justifyContent: 'space-between',
  },
  noPadding: {
    padding: 0,
  },
  infoActions: {
    flexDirection: 'column',
    alignItems: 'flex-end',
  },
});

class RecipeDetails extends Component {
  componentDidMount() {
    const {
      match: { params = {} },
    } = this.props;

    this.props.fetchSingleRecipe(params.id);
  }
  render() {
    const {
      recipe = {},
      classes,
      toggleFavorite = () => {},
      clickRating = () => {},
    } = this.props;

    const time = moment
      .duration(recipe.time, 'minutes')
      .format('h [hrs]: m [min]')
      .toString();

    return (
      <React.Fragment>
        <Grid container spacing={24}>
          <Grid item xs={12} md={6} className={classes.image}>
            <Paper className={classes.image} elevation={1}>
              <img src={recipe.image} alt={recipe.name} />
            </Paper>
          </Grid>
          <Grid item xs={12}>
            <Paper className={classes.paper} elevation={1}>
              <Grid container>
                <Grid item xs={12} className={classes.basicInfo}>
                  <div>
                    <Typography variant="h4">{recipe.name}</Typography>
                    <Typography variant="h6">{recipe.headline}</Typography>
                  </div>
                  <div
                    className={classNames(
                      classes.infoWrapper,
                      classes.infoActions,
                      classes.noPadding
                    )}>
                    <FavoriteButton
                      toggleFavorite={() => toggleFavorite(recipe.id)}
                      ariaLabel={'Add to favorites'}
                      isFavorite={recipe.isFavorite}
                    />
                    <RatingButtons onClick={clickRating} recipe={recipe} />
                  </div>
                </Grid>

                <Grid item xs={12}>
                  <Divider className={classes.divider} />
                </Grid>

                <Grid item xs={12}>
                  <div className={classes.infoWrapper}>
                    <Icon
                      className={classNames(classes.iconLeft, 'fas fa-clock')}
                    />
                    <Typography variant="body1">{time}</Typography>
                  </div>
                </Grid>

                <Grid item xs={12} sm={3}>
                  <div className={classes.infoWrapper}>
                    <Icon
                      className={classNames(classes.iconLeft, 'fas fa-fire')}
                    />
                    <Typography variant="body1">
                      <strong>Calories:</strong> {recipe.calories}
                    </Typography>
                  </div>
                </Grid>
                <Grid item xs={12} sm={3}>
                  <div className={classes.infoWrapper}>
                    <Icon
                      className={classNames(classes.iconLeft, 'fas fa-coffee')}
                    />
                    <Typography variant="body1">
                      <strong>Fats:</strong> {recipe.fats}
                    </Typography>
                  </div>
                </Grid>
                <Grid item xs={12} sm={3}>
                  <div className={classes.infoWrapper}>
                    <Icon
                      className={classNames(
                        classes.iconLeft,
                        'fas fa-cookie-bite'
                      )}
                    />
                    <Typography variant="body1">
                      <strong>Carbs:</strong> {recipe.carbos}
                    </Typography>
                  </div>
                </Grid>
                <Grid item xs={12} sm={3}>
                  <div className={classes.infoWrapper}>
                    <Icon
                      className={classNames(classes.iconLeft, 'fas fa-fish')}
                    />
                    <Typography variant="body1">
                      <strong>Proteins:</strong> {recipe.proteins}
                    </Typography>
                  </div>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
          <Grid item xs={12}>
            <Divider />
          </Grid>
          <Grid item xs={12}>
            <Paper className={classes.paper} elevation={1}>
              <Typography variant="subtitle1">{recipe.description}</Typography>
            </Paper>
          </Grid>
          <Grid item xs={12}>
            <Divider />
          </Grid>
          <Grid item xs={12}>
            <Paper className={classes.paper} elevation={1}>
              <Grid container>
                <Grid item xs={12} sm={6}>
                  <Typography variant="h6" className={classes.titleWrapper}>
                    <Icon
                      className={classNames(classes.iconLeft, 'fas fa-blender')}
                    />
                    Ingredients
                  </Typography>
                  <List>
                    {Array.isArray(recipe.ingredients) &&
                      recipe.ingredients.map((i, k) => {
                        return (
                          <ListItem key={k}>
                            <ListItemText
                              primary={i}
                              // secondary={secondary ? 'Secondary text' : null}
                            />
                          </ListItem>
                        );
                      })}
                  </List>
                </Grid>
                <Grid item xs={12} sm={6}>
                  <Typography variant="h6" className={classes.titleWrapper}>
                    <Icon
                      className={classNames(
                        classes.iconLeft,
                        'fas fa-utensils'
                      )}
                    />
                    How to do it
                  </Typography>
                  <iframe
                    title={recipe.name}
                    style={{ marginTop: 24, width: '100%', height: 390 }}
                    width="560"
                    height="315"
                    src="https://www.youtube.com/embed/d5P8dDMQoYM"
                    frameBorder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowFullScreen
                  />
                </Grid>
              </Grid>
            </Paper>
          </Grid>
        </Grid>
      </React.Fragment>
    );
  }
}

RecipeDetails.propTypes = {
  classes: PropTypes.object.isRequired,
  recipe: PropTypes.shape({
    name: PropTypes.string,
    description: PropTypes.string,
    headline: PropTypes.string,
    isFavorite: PropTypes.bool,
    image: PropTypes.string,
    ingredients: PropTypes.array,
  }),
  toggleFavorite: PropTypes.func.isRequired,
  clickRating: PropTypes.func.isRequired,
};

export default withStyles(styles)(withWidth()(RecipeDetails));
