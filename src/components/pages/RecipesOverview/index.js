import React, { Component } from 'react';
import CardRecipe from '../../molecules/CardRecipe';
import { Grid } from '@material-ui/core';
import PropTypes from 'prop-types';

class RecipesOverview extends Component {
  state = {
    recipes: [],
  };

  componentDidMount() {
    this.props.fetchRecipes();
  }

  render() {
    const {
      toggleFavorite,
      recipes: { items },
    } = this.props;

    return (
      <Grid container spacing={24}>
        {items.map((recipe, i) => {
          return (
            <Grid item xs={12} md={3} key={recipe.id}>
              <CardRecipe
                recipe={recipe}
                toggleFavorite={() => toggleFavorite(recipe.id)}
              />
            </Grid>
          );
        })}
      </Grid>
    );
  }
}

RecipesOverview.propTypes = {
  toggleFavorite: PropTypes.func.isRequired,
  recipes: PropTypes.shape({
    items: PropTypes.array.isRequired,
  }),
};

export default RecipesOverview;
