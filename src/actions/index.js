import { MockData } from '../_data/source';

/**
 *
 * @type {{SHOW_ALL: string, SHOW_FAVORITE: string, SHOW_FILTERED: string}}
 */
const filters = {
  SHOW_ALL: 'SHOW_ALL',
  SHOW_FAVORITE: 'SHOW_FAVORITE',
  SHOW_FILTERED: 'SHOW_FILTERED',
};

/**
 *
 * @type {string}
 */
const FETCH_RECIPES_BEGIN = 'FETCH_RECIPES_BEGIN';
/**
 *
 * @type {string}
 */
const FETCH_RECIPES_SUCCESS = 'FETCH_RECIPES_SUCCESS';
/**
 *
 * @type {string}
 */
const FETCH_RECIPES_FAILURE = 'FETCH_RECIPES_FAILURE';
/**
 *
 * @type {string}
 */
const TOGGLE_FAVORITE = 'TOGGLE_FAVORITE';
/**
 *
 * @type {string}
 */
const FETCH_RECIPE = 'FETCH_RECIPE';
/**
 *
 * @type {string}
 */
const SET_VISIBILITY_FILTER = 'SET_VISIBILITY_FILTER';
/**
 *
 * @type {string}
 */
const CLICK_RATING = 'CLICK_RATING';

/**
 *
 * @param id
 * @returns {{type: string, id: *}}
 */
const toggleFavorite = id => ({
  type: TOGGLE_FAVORITE,
  id,
});

/**
 *
 * @param searchTerm
 * @param isFavoriteSearch
 * @returns {{type: string, filter: string, term: *, isFavoriteSearch: *}}
 */
const search = (searchTerm, isFavoriteSearch) => ({
  type: SET_VISIBILITY_FILTER,
  filter: filters.SHOW_FILTERED,
  term: searchTerm,
  isFavoriteSearch: isFavoriteSearch,
});

/**
 *
 * @param id
 * @returns {Promise<any>}
 */
function fakeGetRecipes(id) {
  const favorites = JSON.parse(window.localStorage.getItem('favorites'));
  let mockmock = MockData;

  return new Promise(resolve => {
    if (id) {
      mockmock = mockmock.filter(m => m.id === id);
    }

    if (Array.isArray(favorites) && favorites.length) {
      mockmock = mockmock.map(m => {
        m.isFavorite = false;
        favorites.map(f => {
          if (f === m.id) {
            m.isFavorite = true;
          }
          return f;
        });
        return m;
      });
    }

    resolve({
      recipes: mockmock,
    });
  });
}

/**
 *
 * @param id
 * @returns {function(*): Promise<{} | never>}
 */
const fetchRecipes = id => {
  return dispatch => {
    dispatch(fetchRecipesBegin());
    return fakeGetRecipes(id)
      .then(result => {
        dispatch(fetchRecipesSuccess(result, id && FETCH_RECIPE));
        return {
          ...result.recipes,
        };
      })
      .catch(error => dispatch(fetchRecipesFailure(error)));
  };
};

/**
 *
 * @param value
 * @returns {{type: string, value: *}}
 */
const clickRating = value => {
  return {
    type: CLICK_RATING,
    value,
  };
};

/**
 *
 * @returns {{type: string}}
 */
const fetchRecipesBegin = () => ({
  type: FETCH_RECIPES_BEGIN,
});

/**
 *
 * @param recipes
 * @param id
 * @returns {{type: string, id: *, payload: {}}}
 */
const fetchRecipesSuccess = (recipes, id) => ({
  type: FETCH_RECIPES_SUCCESS,
  id: id,
  payload: {
    ...recipes,
  },
});

/**
 *
 * @param error
 * @returns {{type: string, payload: {error: *}}}
 */
const fetchRecipesFailure = error => ({
  type: FETCH_RECIPES_FAILURE,
  payload: { error },
});

export {
  toggleFavorite,
  fetchRecipes,
  fetchRecipesBegin,
  fetchRecipesSuccess,
  fetchRecipesFailure,
  clickRating,
  search,
  filters,
  FETCH_RECIPES_BEGIN,
  FETCH_RECIPES_SUCCESS,
  FETCH_RECIPES_FAILURE,
  TOGGLE_FAVORITE,
  FETCH_RECIPE,
  SET_VISIBILITY_FILTER,
  CLICK_RATING,
};
