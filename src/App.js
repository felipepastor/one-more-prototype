import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { createBrowserHistory } from 'history';
import { Router, Route, Switch, Redirect } from 'react-router-dom';

import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import withWidth from '@material-ui/core/withWidth';
import Recipes from './containers/Recipes';
import Recipe from './containers/Recipe';
import Header from './containers/Header';

import './App.css';

const styles = ({ palette, breakpoints, spacing: { unit } }) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: unit * 2,
    textAlign: 'center',
    color: palette.text.secondary,
  },
  mainContainer: {
    maxWidth: 1280,
    margin: 'auto',
    padding: unit * 4,
  },
});

class App extends Component {
  history = createBrowserHistory();

  render() {
    const { classes } = this.props;

    return (
      <Router history={this.history}>
        <div className={classes.root}>
          <Header />
          <main className={classes.mainContainer}>
            <Grid container spacing={24}>
              <Switch>
                <Route path="/recipes" exact component={Recipes} />
                <Route path="/recipes/:id" exact component={Recipe} />
                <Redirect to="/recipes" from="*" />
              </Switch>
            </Grid>
          </main>
        </div>
      </Router>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(withWidth()(App));
