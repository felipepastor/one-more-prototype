import { connect } from 'react-redux';
import { toggleFavorite, filters, fetchRecipes } from '../actions';
import RecipesOverview from '../components/pages/RecipesOverview';

/**
 *
 * @param recipes
 * @param filter
 * @param state
 * @returns {Array}
 */
const getVisibleRecipes = (recipes = [], filter = {}, state) => {
  switch (filter.type) {
    case filters.SHOW_ALL:
      recipes.original = recipes.items;
      return recipes;
    case filters.SHOW_FILTERED:
      const term = filter.term || '';

      recipes.items = recipes.original.filter(t => {
        return t.name.toLowerCase().indexOf(term.toLowerCase()) > -1;
      });

      if (filter.isFavoriteSearch) {
        recipes.items = recipes.items.filter(t => {
          return t.isFavorite === true;
        });
      }

      return recipes;
    default:
      throw new Error('Unknown filter: ' + filter);
  }
};

/**
 *
 * @param state
 * @returns {{recipes: Array, loading: boolean, error: *}}
 */
const mapStateToProps = state => ({
  recipes: getVisibleRecipes(state.recipes, state.filter, state),
  loading: state.recipes.loading,
  error: state.recipes.error,
});

const mapDispatchToProps = dispatch => ({
  toggleFavorite: id => dispatch(toggleFavorite(id)),
  fetchRecipes: () => dispatch(fetchRecipes()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RecipesOverview);
