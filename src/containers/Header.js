import { connect } from 'react-redux';
import { search } from '../actions';
import AppHeader from '../components/organims/AppHeader';

/**
 *
 * @param state
 * @returns {{recipe: (initialState.recipe|{}), isSinglePage: (initialState.recipe|{}|*)}}
 */
const mapStateToProps = (state = {}) => {
  return {
    recipe: state.recipe,
    isSinglePage: !!(state.recipe && state.recipe.id),
  };
};

/**
 *
 * @param dispatch
 * @returns {{search: (function(*=, *=): *)}}
 */
const mapDispatchToProps = dispatch => ({
  search: (searchTerm, isFavoriteSearch) =>
    dispatch(search(searchTerm, isFavoriteSearch)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppHeader);
