import { connect } from 'react-redux';
import { toggleFavorite, fetchRecipes, clickRating } from '../actions';
import RecipeDetails from '../components/pages/RecipeDetails';

/**
 *
 * @param state
 * @param action
 * @returns {{recipe: (initialState.recipe|{})}}
 */
const mapStateToProps = (state = {}, action) => {
  switch (action.type) {
    default:
      return {
        recipe: state.recipe,
      };
  }
};

/**
 *
 * @param dispatch
 * @returns {{fetchSingleRecipe: (function(*=): *), toggleFavorite: (function(*=): *), clickRating: (function(*=, *=): *)}}
 */
const mapDispatchToProps = dispatch => ({
  fetchSingleRecipe: id => dispatch(fetchRecipes(id)),
  toggleFavorite: id => dispatch(toggleFavorite(id)),
  clickRating: (id, value) => dispatch(clickRating(id, value)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RecipeDetails);
